package com.example.lista;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText Etxt;
    private ListView List;
    private ArrayList<String> Lista;
    private ArrayAdapter<String> Adaptador;
    public int Position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Etxt = (EditText) findViewById(R.id.Etxt);
        List = findViewById(R.id.List);
        Position = -1;
        Lista = new ArrayList<>();
        Adaptador = new ArrayAdapter<String>(MainActivity.this, R.layout.activity_color_txt, R.id.TxtV, Lista);
        List.setAdapter(Adaptador);
        List.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(this, ""+List.getFocusable(), Toast.LENGTH_SHORT).show();
                setPos(position);
                List.set.setBackgroundColor(Color.BLUE);
            }
        });
    }

    public void setPos(int Pos){
        Position = Pos;
    }

    public void onAdd (View v){
        if(!Etxt.getText().toString().equals("")){
            Lista.add(Etxt.getText().toString());
            Adaptador.notifyDataSetChanged();
            Etxt.setText("");
            Toast.makeText(this, "El dato se agrego exitosamente", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this, "Favor de ingresar un dato", Toast.LENGTH_SHORT).show();
    }

    public void onDelete(View v){
        if(Position>-1){
            Lista.remove(Position);
            Adaptador.notifyDataSetChanged();
            Toast.makeText(this, "El dato se elimino exitosamente", Toast.LENGTH_SHORT).show();
            Position = -1;
        }else
            Toast.makeText(this, "Elija un item a eliminar", Toast.LENGTH_SHORT).show();
    }

    public void onEdit(View v){
        if(Position>-1&&!Etxt.getText().toString().equals("")){
            Lista.set(Position, Etxt.getText().toString());
            Adaptador.notifyDataSetChanged();
            Etxt.setText("");
            Toast.makeText(this, "El dato se modifico exitosamente", Toast.LENGTH_SHORT).show();
            Position = -1;
        }else
            Toast.makeText(this, "Elija un item a modificar\n o\n escriba la palabra de remplazo", Toast.LENGTH_SHORT).show();
    }
}
